package com.twuc.webApp;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class BookController {

    @GetMapping("/users/{userId}/books")
    public ResponseEntity getBook(@PathVariable Long userId) {
        return ResponseEntity.status(200).contentType(MediaType.TEXT_PLAIN).body(String.format("The book for user %d", userId));
    }

    @GetMapping("/users/{userId}")
    public String getUser(@PathVariable Long UserId) {
        return String.format("user id is %d", UserId);
    }

    @GetMapping("/segments/good")
    public ResponseEntity getFirstPath() {
        return ResponseEntity.status(200).contentType(MediaType.TEXT_PLAIN).body("first good");
    }

    @GetMapping("/segments/{good}")
    public ResponseEntity getSecondPath(@PathVariable String good) {
        return ResponseEntity.status(200).contentType(MediaType.TEXT_PLAIN).body(String.format("second %s", good));
    }

    @PostMapping("/wildcards/?cd")
    public ResponseEntity getTwoChar() {
        return ResponseEntity.status(200).contentType(MediaType.TEXT_PLAIN).body("success");
    }

    @GetMapping("/wildcards/*")
    public String getWildcards() {
        return "successful";
    }

    @GetMapping("/*/wildcards")
    public String getAnyWildcards() {
        return "successful";
    }

    @GetMapping("/wildcard/before/*/after")
    public String getBeforeAfterWildcards() {
        return "successful";
    }

    @GetMapping("/wildcard/*o*/after")
    public String getAnyAfterWildcards() {
        return "match *o*";
    }

    @GetMapping("/wildcard/**/after")
    public String getDoubleWildcards() {
        return "match **";
    }

    @PostMapping("/wildcard/{userId:[\\d]{4}}")
    public String getRegex(@PathVariable Long userId) {
        return String.format("regex %d", userId);
    }

    @GetMapping("/users")
    public String matcherQueryString(@RequestParam Integer id) {
        return String.valueOf(id);
    }

    @GetMapping(value = "/users/test", params = {"!name"})
    public String matcherNotPresentQueryString(@RequestParam Integer id) {
        return String.valueOf(id);
    }

}
