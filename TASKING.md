####2.1 Path Variable 的匹配
1. should return 200 when send request with id.
####2.2 Path Variable 大小写
1. should return 500 when path variable have different case.
####2.3 优先级
1. should return first route when visit the same path.
####2.4 **?** 通配符
1. should return 404 when have two char match. 
1. should return 404 when have no char match. 
####2.5 **\*** 通配符
1. should return 200 when have one segment.
2. should return 200 when have one segment in intermediate.
3. should return 404 when no segment in intermediate.
4. should return 200 when match segment in intermediate.
####2.6 **\*\*** 通配符
1. should return 200 when match double segment in intermediate.
####2.7 正则表达式
1. should return 200 when path use regex.
####2.8 Query String
1. should return 200 when query string routing.
2. should return 400 when url not contains params given request param
3. should return 200 when query string not contains params given request param.
4. should return 500 when query string not contains params given request param1.
5. should return 500 when query string not contains params given request param2.
