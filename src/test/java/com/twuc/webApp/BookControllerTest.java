package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class BookControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_when_send_request_with_id() throws Exception {
        mockMvc.perform(get("/api/users/2/books"))
                .andExpect(status().is(200))
                .andExpect(content().contentType("text/plain"))
                .andExpect(content().string("The book for user 2"));
        mockMvc.perform(get("/api/users/23/books"))
                .andExpect(status().is(200))
                .andExpect(content().contentType("text/plain"))
                .andExpect(content().string("The book for user 23"));
    }

    @Test
    void should_return_500_when_path_variable_have_different_case() throws Exception {
        mockMvc.perform(get("/api/users/2"))
                .andExpect(status().is(500));
    }

    @Test
    void should_return_first_route_when_visit_the_same_path() throws Exception {
        mockMvc.perform(get("/api/segments/good"))
                .andExpect(status().is(200))
                .andExpect(content().contentType("text/plain"))
                .andExpect(content().string("first good"));
    }

    @Test
    void should_return_404_when_have_two_char_match() throws Exception {
        mockMvc.perform(post("/api/wildcards/acd"))
                .andExpect(status().is(200));
        mockMvc.perform(post("/api/wildcards/abcd"))
                .andExpect(status().is(404));
    }

    @Test
    void should_return_404_when_have_no_char_match() throws Exception {
        mockMvc.perform(post("/api/wildcards/acd"))
                .andExpect(status().is(200));
        mockMvc.perform(post("/api/wildcards/"))
                .andExpect(status().is(404));
    }

    @Test
    void should_return_200_when_have_one_segment() throws Exception {
        mockMvc.perform(get("/api/wildcards/anything"))
                .andExpect(status().is(200))
                .andExpect(content().string("successful"));
    }

    @Test
    void should_return_200_when_have_one_segment_in_intermediate() throws Exception {
        mockMvc.perform(get("/api/anything/wildcards"))
                .andExpect(status().is(200))
                .andExpect(content().string("successful"));
    }

    @Test
    void should_return_404_when_no_segment_in_intermediate() throws Exception {
        mockMvc.perform(get("/api/wildcard/before/after"))
                .andExpect(status().is(404));
    }

    @Test
    void should_return_200_when_match_segment_in_intermediate() throws Exception {
        mockMvc.perform(get("/api/wildcard/good/after"))
                .andExpect(status().is(200))
                .andExpect(content().string("match *o*"));
    }

    @Test
    void should_return_200_when_match_double_segment_in_intermediate() throws Exception {
        mockMvc.perform(get("/api/wildcard/good/before/after"))
                .andExpect(status().is(200))
                .andExpect(content().string("match **"));

    }

    @Test
    void should_return_200_when_path_use_regex() throws Exception {
        mockMvc.perform(post("/api/wildcard/1024"))
                .andExpect(status().is(200))
                .andExpect(content().string("regex 1024"));

    }

    @Test
    void should_return_200_when_query_string_routing() throws Exception {
        mockMvc.perform(get("/api/users?id=2"))
                .andExpect(status().is(200));
    }

    @Test
    void should_return_400_when_url_not_contains_params_given_request_param() throws Exception {
        mockMvc.perform(get("/api/users"))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_200_when_query_string_not_contains_params_given_request_param() throws Exception {
        mockMvc.perform(get("/api/users/test?id=3"))
                .andExpect(status().is(200));
    }

    @Test
    void should_return_500_when_query_string_not_contains_params_given_request_param1() throws Exception {
        mockMvc.perform(get("/api/users/test?name=3"))
                .andExpect(status().is(500));
    }

    @Test
    void should_return_500_when_query_string_not_contains_params_given_request_param2() throws Exception {
        mockMvc.perform(get("/api/users/q?id=3&name=3"))
                .andExpect(status().is(500));
    }

}
